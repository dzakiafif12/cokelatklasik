<?php

return [

    'dashboard' => 'Dashboards',
    'items' => 'Items',
    'finance' => 'Finance',
    'report' => 'Reports',
    'recv_report' => 'Receivings',
    'sale_report' => 'Sales',
    'transaction' => 'Transaction',
    'penjualan' => 'Penjualan',
    'setting' => 'Settings'
];
