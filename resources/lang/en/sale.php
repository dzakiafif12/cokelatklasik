<?php

return [

	'sales_register' => 'Goods Distribution',
	'search_item' => 'Search Item:',
	'invoice' => 'Invoice',
	'employee' => 'Employee',
	'payment_type' => 'Payment Type',
	'customer' => 'Customer',
	'item_id' => 'Item ID',
	'item_name' => 'Item Name',
	'price' => 'Price',
	'quantity' => 'Quantity',
	'total' => 'Total',
	'add_payment' => 'Add Payment',
	'comments' => 'Comments',
	'grand_total' => 'TOTAL:',
	'amount_due' => 'Amount Due',
	'submit' => 'Complete',
	//struk
	'sale_id' => 'Sale ID',
	'item' => 'Item',
	'qty' => 'Qty',
	'print' => 'Print',
	'new_sale' => 'New Sale'

];
