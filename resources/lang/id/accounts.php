<?php

return [

    'list_accounts' => 'Daftar Akun',
    'new_accounts' => 'Akun Baru',
    'id' => 'ID',
    'code' => 'Kode Akun',
    'name' => 'Nama',
    'access' => 'access',
    'edit' => 'Ganti',
    'delete' => 'Hapus',
    'update_accounts' => 'Ganti accounts',
    'submit' => 'Submit'
];
