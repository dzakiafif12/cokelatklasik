<?php

return [

	'reports' => 'Laporan',
	'sales_report' => 'Laporan Distribusi Brang',
//	'grand_total' => 'TOTAL',
//	'grand_profit' => 'KEUNTUNGAN',
	'item_id' => 'ID Barang',
	'date' => 'Tanggal',
	'items_solded' => 'Barang yang dijual',
	'sold_by' => 'Dijual Oleh',
	'sold_to' => 'Dijual Kepada',
	'total' => 'Total',
	'profit' => 'Keuntungan',
	'payment_type' => 'Jenis Pembayaran',
	'comments' => 'Komentar',
	'detail' => 'Rincian',
	'item_id' => 'ID Barang',
	'item_name' => 'Nama Barang',
	'quantity_sold' => 'Jumlah yang dijual',

];
