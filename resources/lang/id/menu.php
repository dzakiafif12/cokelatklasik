<?php

return [

	'dashboard' => 'Dashboards',
	'items' => 'Barang',
	'finance' => 'Keuangan',
	'report' => 'Laporan',
	'recv_report' => 'Penerimaan',
	'sale_report' => 'Penjualan',
	'setting' => 'Pengaturan',
	'penjualan' => 'Penjualan',
	'transaction' => 'Transaksi'

];
