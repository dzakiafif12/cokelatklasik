<?php

return [

    'list_customers' => 'Daftar Mitra',
    'new_customer' => 'Mitra Baru',
    'customer_id' => 'ID Mitra',
    'name' => 'Nama',
    'email' => 'Email',
    'phone_number' => 'Nomor Telepon',
    'avatar' => 'Foto',
    'choose_avatar' => 'Pilih Foto',
    'address' => 'Alamat',
    'city' => 'Kota',
    'state' => 'Provinsi',
    'zip' => 'Kode Pos',
    'company_name' => 'Nama Perusahaan',
    'account' => 'Akun',
    'Submit' => 'Submit',
    'is_delete' => 'Is Delete',
    'tipe' => 'Tipe',
    'edit' => 'Ganti',
    'delete' => 'Hapus',
    'update_customer' => 'Ganti Pelanggan',
];
