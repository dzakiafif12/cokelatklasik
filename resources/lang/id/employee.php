<?php

return [

	'list_employees' => 'Daftar Pegawai',
	'new_employee' => 'Pegawai Baru',
	'person_id' => 'ID Pegawai',
	'name' => 'Nama',
	'username' => 'nama pengguna',
	'email' => 'Email',
	'password' => 'Password',
	'confirm_password' => 'Ulangi Password',
	'submit' => 'Submit',
	'edit' => 'Ganti',
	'delete' => 'Hapus',
	'update_employee' => 'Ganti Pegawai'

];
