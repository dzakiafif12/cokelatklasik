@extends('app')

@section('breadcrumb')
<li><a href=""> {{trans('penjualan.new_penjualan')}}</a></li>
@endsection

@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/penjualan.js', array('type' => 'text/javascript')) !!}

<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3 style="text-transform:capitalize"><i class="icon icon-grid-lines-streamline"></i> {{trans('transaction.penjualan')}}</h3>
            <p>&nbsp;</p>
            <div class="panel panel-default">
                {{-- < div class = "panel-heading" > < span class = "glyphicon glyphicon-inbox" aria - hidden = "true" > < /span> {{trans('transactions.transactions_register')}}</div>--}}

            <div class="panel-body">

                @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                {!! Html::ul($errors->all()) !!}

                <div class="row" ng-controller="penjualanCtrl">
                    <div class="panel-body" ng-hide="is_edit"  st-pipe="callServer" st-table="displayed">
                        <button class="btn btn-small btn-success" ng-click="create()" ng-show="is_edit == false" style="text-transform:capitalize">{{trans('transaction.new_penjualan')}}</button>

                        <hr />

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Kode Transaksi</td>
                                    <td>Tanggal Transaksi</td>
                                    <td>Outlet</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="det in displayed">
                                    <td>@{{det.id}}</td>
                                    <td>@{{det.code}}</td>
                                    <td>@{{det.transactions_date| date}}</td>
                                    <td>@{{det.outlet}}</td>
                                    <td>
                                        <button ng-click="update(det)" class="btn btn-small btn-info"><i class="fa fa-pencil"></i> {{trans('transaction.edit')}}</button>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body" ng-show="is_edit && !is_print">
                        <form name="KomalForm">
                            <div class="col-md-3">
                                <label>{{trans('sale.item')}} <input ng-model="searchKeyword" class="form-control"></label>

                                <table class="table table-hover">
                                    <tr ng-repeat="item in items| filter: searchKeyword | limitTo:10">

                                        <td>@{{item.id}}</td>
                                        <td>@{{item.item_name}}</td>
                                        <td>
                                            <button class="btn btn-success btn-xs" type="button" ng-click="addSaleTemp(item, newsaletemp)">
                                                <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-9">
                                <br/>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Nama Outlet</label>

                                    <div class="col-sm-9">
                                        <select ng-change="setOutlet(form, form.department_id)" class="form-control" ng-disabled="is_view" ng-model="form.department_id" required>
                                            <option value="undefined">Pilih Outlet</option>
                                            <option ng-repeat="list in listDepartment" ng-if="list.tipe == 'outlet'" value="@{{list.id}}">@{{list.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <div class="row">

                                        <!--{!! Form::open(array('url' => 'sales', 'class' => 'form-horizontal')) !!}-->

                                        <table class="table table-bordered col-md-9">
                                            <thead>
                                                <tr>
                                                    <th>ID akun</th>
                                                    <th>Nama Barang</th>
                                                    <th>Terpakai</th>
                                                    <th>Sisa</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="det in transDetail">
                                                    <td>@{{det.item_id}}</td>
                                                    <td>@{{det.item_name}}</td>
                                                    <td><input class="form-control" type="number" name="qty[]" ng-model="det.qty" size="7" required title="Data yang anda masukkan kurang lengkap" x-moz-errormessage="Data yang anda masukkan kurang lengkap"></td>
                                                    <td><input class="form-control" type="number" name="sisa[]" ng-model="det.sisa" size="7" required title="Data yang anda masukkan kurang lengkap" x-moz-errormessage="Data yang anda masukkan kurang lengkap"></td>
                                                    <td>
                                                        <button class="btn btn-danger btn-xs" type="button" ng-click="removeRow($index)">
                                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="total" class="col-sm-4 control-label">{{trans('sale.add_payment')}}</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">Rp</div>
                                                            <input type="angka" class="form-control" id="add_payment" ng-model="form.setor" required title="Data yang anda masukkan kurang lengkap" x-moz-errormessage="Data yang anda masukkan kurang lengkap" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>&nbsp;</div>
                                                <div class="form-group">
                                                    <label for="employee" class="col-sm-4 control-label">Keterangan</label>
                                                    <div class="col-sm-8">
                                                        <textarea class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
<!--                                                <div class="form-group">
                                                    <label for="supplier_id" class="col-sm-4 control-label">Setoran Tunai</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><b>@{{sum(saletemp) | currency}}</b></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="amount_due" class="col-sm-4 control-label">{{trans('sale.amount_due')}}</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static">@{{add_payment - sum(saletemp) | currency}}</p>
                                                    </div>
                                                </div>-->
                                                <p>&nbsp;</p>
                                                <span class="form-group">
                                                    <div class="col-sm-2"><button type="submit" ng-click="cancle()" class="btn ">Back</button></div>
                                                    <div class="col-sm-6">
                                                        <button type="submit" ng-click="save(form, transDetail)" class="btn btn-success btn-block"><i class="fa fa-fw fa-check"></i> {{trans('sale.submit')}}</button>
                                                    </div>
                                                </span>

                                            </div>
                                        </div>

                                        <!--                            {!! Form::close() !!}-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9">


                            </div>
                        </form>
                    </div>
                    <div ng-show="is_print" class="panel-body">
                        <div id="printed-area">
                            <div class="container-fluid" style="border: 1px solid #ddd; padding: 25px; margin:25px;">
                                <div class="col-md-12" style="text-align:center">
                                    <h2 style="font-weight: 700;">COKELAT KLASIK PASURUAN</h2>
                                    <h3 style="margin-top:5px">Laporan Penjualan</h3>
                                </div>
                                <hr/>
                                <br><br>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-5" style="text-transform:capitalize">
                                            Tanggal : {{ date('d M Y') }}
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-5" style="text-transform: capitalize;">
                                            Gerai / Shift : @{{form.outlet}}
                                        </div>
                                    </div>

                                <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table-bordered table-striped table">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nama Bahan</th>
                                                    <th>Terpakai</th>
                                                    <th>Sisa</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="det in transDetail">
                                                    <td>@{{$index + 1}}</td>
                                                    <td>@{{det.item_name}}</td>
                                                    <td>@{{det.qty}}</td>
                                                    <td>@{{det.sisa}}</td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="text-transform: uppercase">
                                        <div class="row" style="margin-top: 5px;">
                                            <div class="col-md-9">
                                                SETORAN : rp. @{{form.setor }}
                                            </div>
                                        </div><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-1"><button type="submit" ng-click="cancle()" class="btn pull-right">Back</button></div>
                                    <div class="col-sm-11">
                                        <button type="submit" onclick="print('printed-area')" class="btn btn-success hidden-print pull-right"><i class="fa fa-print"></i> Cetak Laporan</button>


                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function print(printed) {
        var isi = document.getElementById(printed);
        var popupWin = window.open('', '_blank', 'width=1000,height=700');
        popupWin.document.open()
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/global-style.css') }}" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '<br><br></body></html>');
        popupWin.document.close();
    }
</script>
</div>
@endsection
