@extends('app')
@section('breadcrumb')
<li>Report</li>
<li><a href=""> {{trans('report-receiving.receivings_report')}}</a></li>
@endsection
@section('content')
<div id="printed-area">
	<div class="container-fluid" style="text-transform: capitalize">
		<div class="row">
			<div class="col-md-11">
				<h2 style="font-weight: 700; text-align: center;">COKELAT KLASIK PASURUAN</h2>
				<div>
					<h3 id="tengah"><i class="icon icon-grid-lines-streamline"></i> {{trans('report-receiving.receivings_report')}}</h3>
					<div class="panel-body">
						<div class="col-sm-10">
							<div class="well well-sm">{{trans('report-receiving.grand_total')}}: &nbsp; &nbsp; &nbsp;Rp {{DB::table('receiving_items')->sum('total_cost')}}
							</div>
						</div>
						<div class="col-sm-2">
							<button type="button" onclick="print('printed-area')" class="btn btn-success pull-right hidden-print" style="margin-right: 5px; width: 100%;vertical-align: middle"><i class="fa fa-print"></i> {{trans('receiving.print')}}</button>
						</div>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<td>{{trans('report-receiving.receiving_id')}}</td>
									<td>{{trans('report-receiving.date')}}</td>
									<td>{{trans('report-receiving.items_received')}}</td>
									<td>{{trans('report-receiving.received_by')}}</td>
									<td>{{trans('report-receiving.supplied_by')}}</td>
									<td>{{trans('report-receiving.total')}}</td>
									<td>{{trans('report-receiving.payment_type')}}</td>
									<td>{{trans('report-receiving.comments')}}</td>
									<td class="hidden-print">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								@foreach($receivingReport as $value)
								<tr>
									<td>{{ $value->id }}</td>
									<td>{{ $value->created_at }}</td>
									<td>{{DB::table('receiving_items')->where('receiving_id', $value->id)->sum('quantity')}}</td>
									<td>{{ $value->user->name }}</td>
									<td>{{ $value->supplier->company_name }}</td>
									<td>{{DB::table('receiving_items')->where('receiving_id', $value->id)->sum('total_cost')}}</td>
									<td>{{ $value->payment_type }}</td>
									<td>{{ $value->comments }}</td>
									<td class="hidden-print">
										<a class="btn btn-small btn-info" data-toggle="collapse" href="#detailedReceivings{{ $value->id }}" aria-expanded="false" aria-controls="detailedReceivings">{{trans('report-receiving.detail')}}</a>
									</td>
								</tr>
								
								<tr class="collapse" id="detailedReceivings{{ $value->id }}">
									<td colspan="9">
										<table class="table">
											<tr>
												<td>{{trans('report-receiving.item_id')}}</td>
												<td>{{trans('report-receiving.item_name')}}</td>
												<td>{{trans('report-receiving.item_received')}}</td>
												<td>{{trans('report-receiving.total')}}</td>
											</tr>
											@foreach(ReportReceivingsDetailed::receiving_detailed($value->id) as $receiving_detailed)
											<tr>
												<td>{{ $receiving_detailed->item_id }}</td>
												<td>{{ $receiving_detailed->item->item_name }}</td>
												<td>{{ $receiving_detailed->quantity }}</td>
												<td>{{ $receiving_detailed->quantity * $receiving_detailed->cost_price}}</td>
											</tr>
											@endforeach
										</table>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function print(printed) {
var isi = document.getElementById(printed);
var popupWin = window.open('', '_blank', 'width=1000,height=700');
popupWin.document.open()
popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/global-style.css') }}" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
popupWin.document.close();
}
</script>
@endsection