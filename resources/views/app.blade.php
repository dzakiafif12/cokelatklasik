<!DOCTYPE html>
<html lang="en"  ng-app="cokelatklasik">
    <head>
        <meta charset="UTF-8">
        <title>Cokelat Klasik - Home</title>
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/global_style.css') }}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    </head>
    <body>
        <div id="wrapper">
            <div id="sidebar-wrapper">
                @if (Auth::check())
                <ul class="sidebar-nav">
                    <li class="top">
                        <img src="{{ asset('assets/img/mbiz.png') }}" width="100">
                    </li>
                    <li class="">
                        <a href="{{ url('/')}}"><i class="icon icon-3x icon-dashboard-speed-streamline"></i>
                            <p>{{trans('menu.dashboard')}}</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/items')}}">
                            <i class="icon icon-3x icon-factory-lift-streamline-warehouse"></i>
                            <p>{{trans('menu.items')}}</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/transactions')}}">
                            <i class="icon icon-3x icon-photo-pictures-streamline"></i>
                            <p>{{ trans('menu.finance') }}</p>
                        </a>
                    </li>
                    <div id="mn">
                        <li>
                        <a href="#laporan_drop" data-toggle="collapse" data-parent="#mn" role="button" aria-expanded="false"><i class="icon icon-3x icon-receipt-shopping-streamline"></i><p>{{trans('menu.report')}}</p> <span class="fa fa-fw fa-chevron-down"></span></a>
                        <ul class="collapse" role="menu" id="laporan_drop">
                            <li><a href="{{ url('/reports/receivings')}}">{{trans('menu.recv_report')}}</a></li>
                            <li><a href="{{ url('/reports/sales')}}">Distribusi Barang</a></li>
                            <li><a href="{{ url('/reports/transaction/12')}}">{{trans('menu.transaction')}}</a></li>
                            <li><a href="{{ url('/reports/penjualan/12')}}">{{trans('menu.penjualan')}}</a></li>
                            <li><a href="{{ url('/report')}}">Grafik Penjualan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#setting-drop" data-toggle="collapse" data-parent="#mn" role="button" aria-expanded="false"><i class="icon icon-3x icon-settings-streamline-1"></i><p>{{trans('menu.setting')}}</p> <span class="fa fa-fw fa-chevron-down"></span></a>
                        <ul class="collapse" role="menu" id="setting-drop">
                            <li><a href="{{ url('/tutapos-settings')}}">Aplikasi</a></li>
                            <li><a href="{{ url('/employees')}}">Pegawai</a></li>
                        </ul>
                        {{-- <a href="{{ url('/tutapos-settings')}}">
                            <i class="icon icon-3x icon-settings-streamline-1"></i>
                            <p>{{ trans('menu.setting') }}</p>
                        </a> --}}
                    </li>
                    </div>
                </ul>
                @endif
            </div>
            <div id="page-content-wrapper">
                <div class="navbar">
                    <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" class="logo"></a>
                    <ul class="nav pull-right">
                        <li class="profile dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('assets/img/1.jpg') }}">
                                <span style="margin-left: 10px;text-transform: capitalize">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu absolute-dropdown">
                                <li><a href="{{ url('auth/logout') }}">Logout <i class="glyphicons glyphicons-power"></i></a></li>
                            </ul>
                        </li>
                        <li>&nbsp;</li>
                    </ul>
                </div>
                <!--TOOGLE BUTTON-->
                <a href="#menu-toggle" class="btn btn-default menu-togel" id="menu-toggle" style="border-radius: 0;">
                    <i class="glyphicons glyphicons-chevron-left" id="menu-toggle"></i>
                </a>
                <div class="inner-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5">

                                <h4 style="margin-left: 15px;"><i class="fa fa-calendar" style="margin-right: 20px;"></i> {{ date('d M Y') }}</h4>
                                <hr class="cal-hr"><br>
                            </div>
                        </div>


                            @yield('content')

                    </div>
                </div>

            </div>
        </div>
    </body>

    
    <script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

        if ($('#wrapper').hasClass('toggled')) {
            $('i#menu-toggle').removeClass('glyphicons-chevron-left');
            $('i#menu-toggle').addClass('glyphicons-chevron-right');
        } else {
            $('i#menu-toggle').removeClass('glyphicons-chevron-right');
            $('i#menu-toggle').addClass('glyphicons-chevron-left');
        }

    });
    </script>
</html>
