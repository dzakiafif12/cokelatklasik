@extends('app')

@section('breadcrumb')
		<li><a href=""> Settings</a></li>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<h3><i class="icon icon-grid-lines-streamline"></i> Application Settings</h3>
			<br>
			<div class="panel panel-default">


				<div class="panel-body">
					@if (Session::has('message'))
                    	<div class="alert alert-info">{{ Session::get('message') }}</div>
                	@endif
					{!! Html::ul($errors->all()) !!}

					{!! Form::model($tutapos_settings, array('route' => array('tutapos-settings.update', $tutapos_settings->id), 'method' => 'PUT', 'files' => true)) !!}

					<div class="form-group">
					{!! Form::label('languange', 'Languange') !!}
					{!! Form::select('languange', array('en' => 'English', 'id' => 'Indonesia'), Input::old('languange'), array('class' => 'form-control')) !!}
					</div>

					{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}

					{!! Form::close() !!}
				</div>
			</div><br>
			<img src="{{ asset('assets/img/mastej-logo.png') }}" class="mastej-logo"><br>
			<div style="padding-left: 20px;text-align: left;float: none;">
				<strong>
					Jl. Delima Kav. 66 Dermo Mulyoagung Dau Malang<br>
					<i class="fa fa-phone fa-fw"></i>  082244408688<br>
					<i class="fa fa-envelope-o fa-fw"></i>  reza&#64;themastej.com<br>
					<i class="fa fa-tag fa-fw"></i>  www.themastej.com
				</strong>
			</div>
		</div>
	</div>
</div>
@endsection