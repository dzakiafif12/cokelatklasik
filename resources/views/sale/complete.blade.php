@extends('app')
@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/app.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/sale.js', array('type' => 'text/javascript')) !!}
<style>
    table td {
        border-top: none !important;
    }
</style>
<div id="printed-area">
    <div class="container-fluid" style="border: 1px solid #ddd;padding: 25px;">
        <div class="row">
            <div class="col-md-12" style="text-align:center">
                <h2 style="font-weight: 700;">COKELAT KLASIK PASURUAN</h2>
                <hr style="margin-top:10px;margin-bottom:10px">
                <h3 style="margin-top:5px">Laporan Distribusi</h3>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12" style="text-transform: capitalize;">
                Tanggal : {{ date('d M Y') }} <br /><br />

                Outlet : {{ \App\Department::where('id', $sales->customer_id)->value('name') }}<br/>
                Petugas : {{$sales->user->name}}<br />
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>{{trans('sale.item')}}</th>
                                    <th>{{trans('sale.price')}}</th>
                                    <th>{{trans('sale.qty')}}</th>
                                    <th>{{trans('sale.total')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($saleItems as $value) {
                                    echo '<tr>
                                            <td>' . $value->item->item_name . '</td>
                                            <td>Rp. ' . $value->selling_price . '</td>
                                            <td>' . $value->quantity . '</td>
                                            <td>Rp. ' . ((float) $value->selling_price * (float) $value->quantity). '</td>
                                        </tr>';
                                    $total += ((float) $value->selling_price * (float) $value->quantity);
                                }
                                ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">Total</td>
                                    <td>Rp. <?= $total?>,-</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{trans('sale.payment_type')}}: {{$sales->payment_type}}
            </div>
        </div>
        <!--  <hr class="hidden-print"/> -->
        <div class="row hidden-print">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-4">
                <a href="{{ url('/sales') }}" type="button" class="btn btn-info pull-right hidden-print"><i class="fa fa-file-o"></i> {{trans('sale.new_sale')}}</a>
                <button type="button" onclick="print('printed-area')" class="btn btn-success pull-right hidden-print" style="margin-right: 5px;"><i class="fa fa-print"></i> {{trans('sale.print')}}</button>

            </div>
        </div>
    </div>
</div>
<script>
    function print(printed) {
        var isi = document.getElementById(printed);
        var popupWin = window.open('', '_blank', 'width=1000,height=700');
        popupWin.document.open()
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/print.css" /><link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="assets/css/global-style.css" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
        popupWin.document.close();
    }
</script>
@endsection