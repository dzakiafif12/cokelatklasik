@extends('app')
@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/app.js', array('type' => 'text/javascript')) !!}
<style>
table td {
    border-top: none !important;
}
</style>
<div id="printed-area">
    <div class="container-fluid" style="border: 1px solid #ddd;padding: 25px;">
        <div class="row">
            <div class="col-md-12" style="text-align:center">
                <h2 style="font-weight: 700;">COKELAT KLASIK PASURUAN</h2>
                <hr style="margin-top:10px;margin-bottom:10px">
                <h3 style="margin-top:5px">Laporan Penerimaan</h3>
            </div>
        </div><p>&nbsp;</p>
        <div class="row">
            <div class="col-md-12" style="padding-left:25px;text-transform: capitalize">
                Tanggal : {{ date('d M Y') }} <br /><br />
                {{trans('receiving.supplier')}}: {{ $receivings->supplier->company_name}}<br />
                {{trans('receiving.receiving_id')}}: RECV{{$receivingItemsData->receiving_id}}<br />
                {{trans('receiving.employee')}}: {{$receivings->user->name}}<br />
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>{{trans('receiving.item')}}</td>
                                <td>{{trans('receiving.price')}}</td>
                                <td>{{trans('receiving.qty')}}</td>
                                <td>{{trans('receiving.total')}}</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total=0;
                            foreach($receivingItems as $value){
                               echo'
                               <tr>
                                    <td>'.$value->item->item_name.'</td>
                                    <td>Rp.'.$value->cost_price.'</td>
                                    <td>'.$value->quantity.'</td>
                                    <td>Rp.'.$value->total_cost.'</td>
                                </tr>';
                                $total+=$value->total_cost;
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3">Total</td>
                                <td>Rp. <?= $total?>,-</td>
                            </tr>
                            </tfoot>
                        </table>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{trans('receiving.payment_type')}}: {{$receivings->payment_type}}
            </div>
        </div><br>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-2" style="padding-right:0">
                <button type="button" onclick="printInvoice('printed-area')" class="btn btn-info pull-right hidden-print">
                    <i class="fa fa-print"></i> {{trans('receiving.print')}}</button>
            </div>
            <div class="col-md-2">
                <a href="{{ url('/receivings') }}" type="button" class="btn btn-success hidden-print">{{trans('receiving.new_receiving')}}</a>
            </div>
        </div>
    </div>
</div>
<script>
function printInvoice(printed) {
    var isi = document.getElementById(printed);
    var popupWin = window.open('', '_blank', 'width=1000,height=700');
    popupWin.document.open()
    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/print.css" /><link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="assets/css/global-style.css" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
    popupWin.document.close();
}
</script>
@endsection
