@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> {{trans('accounts.new_accounts')}}</h3>
            <div class="panel panel-default">
                {{--<div class="panel-heading">Hak Akses</div>--}}

                <div class="panel-body">

                    {{--<hr />--}}
                    @if (Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    {!! Html::ul($errors->all()) !!}

                    {!! Form::open(array('url' => 'accounts', 'files' => true)) !!}
                    <div class="form-group">
                        {!! Form::label('code', trans('accounts.code').' *') !!}
                        {!! Form::text('code', Input::old('code'), array('class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('name', trans('accounts.name').' *') !!}
                        {!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
                    </div>
                    {!! Form::submit(trans('accounts.submit'), array('class' => 'btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection