@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> {{trans('accounts.list_accounts')}}</h3>
            <div class="panel panel-default">
                {{--<div class="panel-heading">{{trans('accounts.list_accounts')}}</div>--}}

                <div class="panel-body">
                    <a class="btn btn-small btn-success" href="{{ URL::to('accounts/create') }}">{{trans('accounts.new_accounts')}}</a>
                    <hr />
                    @if (Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td>{{trans('accounts.id')}}</td>
                                <td>{{trans('accounts.name')}}</td>
                                <td style="max-width: 20% !important"></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($accounts as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>

                                    <a class="btn btn-small btn-info" href="{{ URL::to('accounts/' . $value->id . '/edit') }}" title="{{trans('accounts.edit')}}"><i class="fa fa-pencil"></i></a>
                                    {!! Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'pull-right')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::submit(trans('accounts.delete'), array('class' => 'btn btn-warning')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection