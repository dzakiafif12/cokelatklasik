<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    public function account()
    {
        return $this->oneToMany('App\TransactionsDetail');
    }
}
