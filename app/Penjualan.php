<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{

    //
    public $table = 'penjualan';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

}
