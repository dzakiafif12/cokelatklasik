<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualan;
use App\Item;
use App\Http\Requests;
use App\Http\Requests\PenjualanRequest;
use App\Http\Controllers\Controller;
use \Auth,
    \Log,
    \Response;

class PenjualanController extends Controller
{

    /**
     * Display a listing of the resource.

     * @return Response
     */
    public function __construct()
    {
        $this->middleware('roles');
    }

    public function index()
    {
        //
        return view('penjualan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function lists($id)
    {

        $models = [];
        $listPenjualan = Penjualan::get();
        if (!empty($listPenjualan)) {
            foreach ($listPenjualan as $key => $val) {
                $models[$key] = $val;
                $models[$key]['outlet'] = (!empty($val->department->name))? $val->department->name : '';
            }
        }
        return Response::json($models);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PenjualanRequest $request)
    {
        //
        $params = json_decode($request->getContent());
        $form = $params->form;
        $detail = $params->detail;

        $lastCode = Penjualan::orderBy('code', 'desc')->first();
        $code = '';
        if (empty($lastCode)) {
            $code = 'JUAL000001';
        } else {
            $urut = ((int) substr($lastCode->code, -6)) + 1;
            $code = 'JUAL' . substr('000000' . $urut, -6);
        }
        $model = new Penjualan;
        $model->code = $code;
        $model->transactions_date = date('Y-m-d');
        $model->user_id = Auth::user()->id;
        $model->department_id = $form->department_id;
        $model->setor = $form->setor;
        if ($model->save()) {
            foreach ($detail as $key => $val) {
                $detail = new \App\PenjualanDetails;
                $detail->penjualan_id = $model->id;
                $detail->qty = $val->qty;
                $detail->sisa = $val->sisa;
                $detail->item_id = $val->item_id;
                if ($detail->save()) {
                    $item = Item::find($detail->item_id);
                    if (!empty($item)) {
                        $item->quantity = $item->quantity + $detail->sisa;
                        $item->save();
                    }
                }
            }

            echo json_encode(array('status' => 1), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0), JSON_PRETTY_PRINT);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function view($id)
    {
        //
        $data = \App\PenjualanDetails::
                where('penjualan_id', $id)
                ->get();
        $results = [];
        foreach ($data as $key => $val) {
            $results[$key] = $val;
            $results[$key]['item_name'] = $val->item->item_name;
        }
        return Response::json($results);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(PenjualanRequest $request, $id)
    {
        //
        $params = json_decode($request->getContent());
        $form = $params->form;
        $detail = $params->detail;
        $model = Penjualan::find($id);

        $model->transactions_date = date('Y-m-d');
        $model->user_id = Auth::user()->id;
        $model->department_id = $form->department_id;
        $model->setor = $form->setor;
        if ($model->save()) {
            $awal = 0;
            foreach ($detail as $key => $val) {
                $detail = \App\PenjualanDetails::find($val->id);
                if(!empty($detail)){
                    $awal = (int) $detail->sisa;
                }
                
                if(empty($detail))
                    $detail = new \App\PenjualanDetails;
                
                
                $detail->penjualan_id = $model->id;
                $detail->qty = $val->qty;
                $detail->sisa = $val->sisa;
                $detail->item_id = $val->item_id;
                if ($detail->save()) {
                    $item = Item::find($detail->item_id);
                    if (!empty($item)) {
                        $item->quantity = $item->quantity + $val->sisa - $awal;
                        $item->save();
                    }
                }
                $awal = 0;
            }

            echo json_encode(array('status' => 1), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0), JSON_PRETTY_PRINT);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
