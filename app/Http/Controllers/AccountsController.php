<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounts;
use App\Http\Requests\AccountsRequest;
use \Auth,
    \Redirect,
    \Validator,
    \Input,
    \Session,
    \Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $account = Accounts::All();
        if (Auth::user()->isAdmin()) {
            $account = Accounts::All();
        } else {
            $account = Accounts::where('user_id', Auth::user()->id)->get();
        }
        return view('accounts.index')->with('accounts', $account);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(AccountsRequest $request)
    {
        //

        $model = new Accounts();
        $model->code = Input::get('code');
        $model->name = Input::get('name');
        $model->save();

        Session::flash('message', 'You have successfully added Accounts');
        return Redirect::to('accounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function searchAkun(Request $request)
    {
        return $request->all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $account = Accounts::find($id);
        return view('accounts.edit')->with('accounts', $account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $model = Accounts::find($id);
        $model->code = Input::get('code');
        $model->name = Input::get('name');
        $model->save();

        Session::flash('message', 'You have successfully added Accounts');
        return Redirect::to('accounts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try
        {
            $account = Accounts::find($id);
            $account->delete();
            Session::flash('message', 'You have successfully deleted supplier');
            return Redirect::to('accounts');
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            Session::flash('message', 'Integrity constraint violation: You Cannot delete a parent row');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::to('accounts');
        }
    }

}
