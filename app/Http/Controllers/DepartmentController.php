<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Department;
use App\Http\Requests\DepartmentRequest;
use \Auth,
    \Redirect,
    \Validator,
    \Input,
    \Response,
    \Session;
use Image;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $departments = Department::get();
        } else {
            $departments = Department::where('user_id', Auth::user()->id)
                    ->get();
        }

        return view('department.index')->with('department', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(DepartmentRequest $request)
    {
        $departments = new Department;
        $departments->name = Input::get('name');
        $departments->email = Input::get('email');
        $departments->phone = Input::get('phone');
        $departments->address = Input::get('address');
        $departments->tipe = Input::get('tipe');
        $departments->user_id = Auth::user()->id;
        $departments->save();

        Session::flash('message', 'You have successfully added department');
        return Redirect::to('department');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function lists($id)
    {
        //
        if (Auth::user()->isAdmin()) {
            $department = Department::where('tipe','outlet')->get();
        } else {
            $department = Department::where('user_id', Auth::user()->id)
                    ->where('tipe','outlet')
                    ->get();
        }
        return Response::json($department);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $departments = Department::find($id);
        return view('department.edit')
                        ->with('department', $departments);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(DepartmentRequest $request, $id)
    {
        $departments = Department::find($id);
        $departments->name = Input::get('name');
        $departments->email = Input::get('email');
        $departments->phone = Input::get('phone');
        $departments->address = Input::get('address');
        $departments->tipe = Input::get('tipe');
        $departments->user_id = Auth::user()->id;
        $departments->save();

        Session::flash('message', 'You have successfully updated department');
        return Redirect::to('department');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $departments = Department::find($id);
            $departments->delete();
            Session::flash('message', 'You have successfully deleted department');
            return Redirect::to('department');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message', 'Integrity constraint violation: You Cannot delete a parent row');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::to('department');
        }
    }

}
