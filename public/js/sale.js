(function () {
    var app = angular.module('cokelatklasik', []);

    app.controller("SearchItemCtrl", ['$scope', '$http', function ($scope, $http) {
            $scope.items = [];
            $http.get('api/item').success(function (data) {
                $scope.items = data;
            });
            $scope.saletemp = [];
            $scope.newsaletemp = {};
            $http.get('api/saletemp').success(function (data, status, headers, config) {
                $scope.saletemp = data;
                $scope.sumTotal($scope.saletemp);
                $scope.sum($scope.saletemp);
            });
            $scope.addSaleTemp = function (item, newsaletemp, customer_id) {
//            console.log(customer_id);
                $http.post('api/saletemp', {item_id: item.id, cost_price: item.cost_price, selling_price: item.selling_price, customer_id: customer_id}).
                        success(function (data, status, headers, config) {
                            $scope.saletemp.push(data);
                            $http.get('api/saletemp').success(function (data) {
                                $scope.saletemp = data;
                                $scope.sumTotal($scope.saletemp);
                                $scope.sum($scope.saletemp);
                            });
                        });
            }
            $scope.updateSaleTemp = function (newsaletemp) {

                $http.put('api/saletemp/' + newsaletemp.id, {quantity: newsaletemp.quantity, total_cost: newsaletemp.item.cost_price * newsaletemp.quantity,
                    total_selling: newsaletemp.item.selling_price * newsaletemp.quantity}).
                        success(function (data, status, headers, config) {

                        });
                $scope.sum($scope.saletemp);
            }
            $scope.removeSaleTemp = function (id) {
                $http.delete('api/saletemp/' + id).
                        success(function (data, status, headers, config) {
                            $http.get('api/saletemp').success(function (data) {
                                $scope.saletemp = data;
                                $scope.sumTotal($scope.saletemp);
                                $scope.sum($scope.saletemp);
                            });
                        });
            }
            $scope.sum = function (list) {
                var total = 0;
                angular.forEach(list, function (val,key) {
                    var sellPrice = (val.selling_price !== undefined || val.selling_price !== null) ? parseFloat(val.selling_price) : 0;
                    var qty = (val.quantity !== undefined || val.quantity !== null) ? parseFloat(val.quantity) : 0;
                    total += parseFloat(sellPrice * qty);
                });
                return total;
            };

            $scope.sumTotal = function (saletemp) {
                var totalSellingPrice = 0;
                var totalCostPrice = 0;
                angular.forEach(saletemp, function (val, key) {
                    totalSellingPrice += (val.selling_price !== undefined || val.selling_price !== null) ? parseInt(val.selling_price) : 0;
                    totalCostPrice += (val.cost_price !== undefined | val.cost_price !== null) ? parseInt(val.cost_price) : 0;
                });
                $scope.totalSellingPrice = totalSellingPrice;
                $scope.totalCostPrice = totalCostPrice;

            }

        }]);
})();
